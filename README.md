# kvs

A toy concurrent key-value store.

My first rust project. I'm using this as a way to learn how to do things in rust and a little about how storage engines are implemented. Any feedback on improving the code/design is appreciated.

## Overview

### Persistance
- `key`s and `value`s are arbitrary byte sequences.
- Writes/Deletes are first logged in a Write Ahead Log (WAL). Then written into an in memory `MemTable` (a concurrent *ordered* map).
- Once the WAL exceeds a certain size, the memtable is frozen and placed into a list of frozen memtables. A new `MemTable` and WAL is created for new writes.
- When the list grows above a certain size, it is flushed to disk as a Sorted String Table (SST).
- When the number of SSTs exceeds a certain threshold, the SSTs are compacted into a single SST. (yes, this is very naive...)
- Flushing and Compaction are done on a single background thread.
- WAL and SST files are kept track of in a file called "version.db".

### Gets/Scans
- Implementations of `trait KvIterator` exist that allow you to iterate over the key-value pairs in the database.
- You can also iterate over key-value pairs that have a specific prefix. Since SSTs are sorted by definition, this is quite easy.
- This is how `get(key)` is implemented: we use an iterator where `prefix` is the `key`.
- When `KvIterator`s are created*, they increment the reference count for tables/ssts in the version file. This stops us from removing these tables/ssts from the version file while the bg thread does the flushing and compaction.
- When the iterator is `drop`ped, it notifies the bg thread so it can update the version file, if it had previously done any flushes or compactions.
- `KvIterator` only supports forward iteration (for now).

\* Actually, only `MergingIterator` increments the reference count, but the `KvIterator`s available to a user just wrap this one.

### Error Recognition/Recovery
I haven't done much on this yet. Maybe some other time...

## Sources
Primary sources I used for learning:
- RocksDB wiki: https://github.com/facebook/rocksdb/wiki
- Justin Jaffray's blog posts on "Durability and Redo Logging" and "Compaction": http://justinjaffray.com/posts/
- Pingcap's Rust course: https://github.com/pingcap/talent-plan/blob/master/courses/rust/docs/lesson-plan.md


