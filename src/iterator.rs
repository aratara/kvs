use crate::kv::DbVersion;
use crate::sst::Sst;
use crate::{DataBlockIndex, InternalKVPair, InternalKey, MemTable, Result};
use crossbeam_skiplist::map::Iter;
use std::mem::ManuallyDrop;
use std::{
    cmp::Ordering,
    fmt::{Display, Formatter},
    fs::File,
    io::{BufReader, Read, Seek, SeekFrom, Write},
    iter::Peekable,
    sync::Arc,
};

pub trait KvIterator {
    fn next(&mut self) -> Option<InternalKVPair>;
    // Moves the cursor to the first `key` that starts with `prefix`.
    // After a successful seek, a subsequent call to `next` will return a key
    // which has a prefix >= the given `prefix`.
    // Returns Ok(true) if the cursor has moved, Ok(false) if it hasn't (an
    // internal detail really...).
    fn seek(&mut self, prefix: &[u8]) -> Result<bool>;
}

// TODO: need to figure out how to compile this when using FnOnce instead
pub struct MergingIterator<'a, F>
where
    F: Fn(),
{
    heap: Vec<CachingIterator<'a>>,
    max_seq: Option<u64>,
    on_close: F,
    // manually drop the arc before calling `on_close` as required for garbage
    // collection
    _hold: ManuallyDrop<Arc<DbVersion>>,
}

impl<'a, F> MergingIterator<'a, F>
where
    F: Fn(),
{
    pub fn new(
        max_seq: Option<u64>,
        version: Arc<DbVersion>,
        on_close: F,
    ) -> Self {
        MergingIterator {
            heap: vec![],
            max_seq,
            on_close,
            _hold: ManuallyDrop::new(version),
        }
    }

    pub fn add_iter(&mut self, iter: impl KvIterator + 'a) {
        let wrapper = CachingIterator::new(iter);
        self.insert(wrapper);
    }

    fn insert(&mut self, iterator: CachingIterator<'a>) {
        if iterator.current.is_some() {
            self.heap.push(iterator);
            self.bubble_up(self.heap.len() - 1);
        }
    }

    fn pop(&mut self) -> Option<InternalKVPair> {
        if self.heap.is_empty() {
            return None;
        }

        // `bubble_down` and `bubble_up` expect the "current" field to be 'Some'
        // after calling `next()` on the wrapper, this might not be the case.
        match self.heap[0].next() {
            Some(result) => {
                if self.heap[0].current.is_none() {
                    self.delete_iterator();
                } else {
                    self.bubble_down(0);
                }
                Some(result)
            }
            None => {
                self.delete_iterator();
                self.pop()
            }
        }
    }

    // deletes the iterator at index 0
    fn delete_iterator(&mut self) {
        let last_idx = self.heap.len() - 1;
        self.heap.swap(0, last_idx);
        self.heap.remove(last_idx);
        self.bubble_down(0);
    }

    #[inline]
    fn bubble_up(&mut self, index: usize) {
        let mut index = index;
        loop {
            if index == 0 {
                break;
            }

            let parent_idx = self.parent(index);
            let parent = &self.heap[parent_idx];
            let child = &self.heap[index];
            let parent_key =
                &parent.current.as_ref().expect("parent to be valid").0;
            let child_key =
                &child.current.as_ref().expect("child to be valid").0;

            if child_key < parent_key {
                self.heap.swap(parent_idx, index);
                index = parent_idx;
            } else {
                break;
            }
        }
    }

    #[inline]
    fn bubble_down(&mut self, index: usize) {
        if self.heap.is_empty() {
            return;
        }

        let mut index = index;
        loop {
            let l_idx = self.left_child(index);
            let r_idx = self.right_child(index);

            if l_idx > self.heap.len() - 1 {
                // No children
                break;
            }

            let parent_key = &self.heap[index]
                .current
                .as_ref()
                .expect("parent to be valid")
                .0;

            if r_idx > self.heap.len() - 1 {
                // Only left child
                let child_key = &self.heap[l_idx]
                    .current
                    .as_ref()
                    .expect("child to be valid")
                    .0;
                if child_key < parent_key {
                    self.heap.swap(index, l_idx);
                    // we don't need to bubble down again because this is the
                    // last leaf.
                }
                break;
            } else {
                // Both left and right child exist.
                let left_key = &self.heap[l_idx]
                    .current
                    .as_ref()
                    .expect("left child to be valid")
                    .0;
                let right_key = &self.heap[r_idx]
                    .current
                    .as_ref()
                    .expect("right child to be valid")
                    .0;

                // to preserve the min-heap property, parent
                // to be smaller than both children.
                if (left_key < right_key) && left_key < parent_key {
                    self.heap.swap(index, l_idx);
                    index = l_idx;
                } else if parent_key > right_key {
                    self.heap.swap(index, r_idx);
                    index = r_idx;
                } else {
                    break;
                }
            }
        }
    }

    #[inline]
    fn parent(&self, index: usize) -> usize {
        (index - 1) / 2
    }

    #[inline]
    fn left_child(&self, index: usize) -> usize {
        (index * 2) + 1
    }

    #[inline]
    fn right_child(&self, index: usize) -> usize {
        (index * 2) + 2
    }
}

impl<'a, F> KvIterator for MergingIterator<'a, F>
where
    F: Fn(),
{
    fn next(&mut self) -> Option<InternalKVPair> {
        if let Some((k, v)) = self.pop() {
            match self.max_seq {
                Some(max_seq) if k.seq > max_seq => {
                    while let Some((k, v)) = self.next() {
                        if k.seq <= max_seq {
                            return Some((k, v));
                        }
                    }
                    return None;
                }
                _ => {
                    return Some((k, v));
                }
            }
        }

        None
    }

    fn seek(&mut self, prefix: &[u8]) -> Result<bool> {
        let mut did_seek = false;
        for iter in self.heap.iter_mut() {
            did_seek = iter.seek(prefix)? || did_seek;
        }

        let to_insert: Vec<CachingIterator<'a>> =
            self.heap.drain(0..).filter(|x| x.current.is_some()).collect();

        for iter in to_insert {
            self.insert(iter);
        }

        Ok(did_seek)
    }
}

impl<'a, F> Drop for MergingIterator<'a, F>
where
    F: Fn(),
{
    fn drop(&mut self) {
        unsafe { ManuallyDrop::drop(&mut self._hold) };
        (self.on_close)();
    }
}

// Caches the InternalKVPair if it exists.
// This allows the MergingIterator to perform comparisons to order its heap.
struct CachingIterator<'a> {
    inner: Box<dyn KvIterator + 'a>,
    current: Option<InternalKVPair>,
}

impl<'a> CachingIterator<'a> {
    fn new(inner: impl KvIterator + 'a) -> Self {
        let mut inner = inner;
        let current = inner.next();
        CachingIterator { inner: Box::new(inner), current }
    }
}

impl<'a> Display for CachingIterator<'a> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match &self.current {
            Some(pair) => write!(f, "Current: {:?}", pair.0),
            None => write!(f, "Current: None"),
        }
    }
}

impl<'a> KvIterator for CachingIterator<'a> {
    fn next(&mut self) -> Option<InternalKVPair> {
        std::mem::replace(&mut self.current, self.inner.next())
    }

    fn seek(&mut self, prefix: &[u8]) -> Result<bool> {
        if let Some(ref kv) = self.current {
            if prefix_cmp(kv.0.user_key.as_slice(), prefix).is_eq() {
                return Ok(false);
            }
        }

        match self.inner.seek(prefix) {
            Ok(pos_moved) => {
                if pos_moved {
                    self.next();
                }
                Ok(pos_moved)
            }
            Err(e) => Err(e),
        }
    }
}

pub struct SstIter<'a> {
    reader: BufReader<File>,
    index: DataBlockIndex,
    iter: Peekable<std::slice::Iter<'a, (InternalKey, (u64, u64))>>,
    data_block_iter: Option<BlockIter>,
    completed: bool,
}

impl<'a> SstIter<'a> {
    pub fn new(sst: &Sst) -> Self {
        let file = File::open(sst.path.as_path())
            .expect("Could not open sst file for SstIter");

        let index = sst.index.clone();
        // SAFETY: the data in Vec is actually a heap pointer; the data is
        // also immutable. Even if Vec gets moved around, the Iter should be
        // operating on valid data.
        let iter = unsafe {
            std::slice::from_raw_parts(index.as_ptr(), index.len())
                .iter()
                .peekable()
        };

        SstIter {
            reader: BufReader::new(file),
            index,
            iter,
            data_block_iter: None,
            completed: false,
        }
    }
}

impl<'a> KvIterator for SstIter<'a> {
    fn next(&mut self) -> Option<InternalKVPair> {
        if self.completed {
            return None;
        }

        if let Some(ref mut block) = self.data_block_iter {
            if let Some(entry) = block.next() {
                return Some(entry);
            }
        }

        match self.iter.next() {
            Some((_, (offset, size))) => {
                let mut buffer: Vec<u8> = Vec::with_capacity(*size as usize);
                buffer.spare_capacity_mut();
                unsafe {
                    buffer.set_len(*size as usize);
                }

                if self.reader.seek(SeekFrom::Start(*offset)).is_err() {
                    eprintln!("Couldn't seek SstIndexIter");
                    self.completed = true;
                    return None;
                }

                if self.reader.read_exact(&mut buffer).is_err() {
                    eprintln!("Couldn't read_exact SstIndexIter");
                    self.completed = true;
                    return None;
                }

                self.data_block_iter = Some(BlockIter::new(buffer));
                self.next()
            }
            None => {
                self.completed = true;
                None
            }
        }
    }

    fn seek(&mut self, prefix: &[u8]) -> Result<bool> {
        // TODO: We can't check the opposite bounds:
        // key > self.index.last().unwrap().0.user_key.as_slice()
        // because we only know the _first_ key in the last data block
        if prefix_cmp(self.index.first().unwrap().0.user_key.as_slice(), prefix)
            .is_gt()
        {
            self.completed = true;
            return Ok(false);
        }

        // try find data block that might contain the key
        let mut result = None;
        while let Some(thing) = self
            .iter
            .next_if(|(k, _)| prefix_cmp(k.user_key.as_slice(), prefix).is_lt())
        {
            result = Some(thing);
        }

        if let Some((_, (offset, size))) = result {
            let mut buffer: Vec<u8> = Vec::with_capacity(*size as usize);
            buffer.spare_capacity_mut();
            unsafe {
                buffer.set_len(*size as usize);
            }

            if self.reader.seek(SeekFrom::Start(*offset)).is_err() {
                eprintln!("Couldn't seek SstIndexIter");
                self.completed = true;
                return Ok(true);
            }

            if self.reader.read_exact(&mut buffer).is_err() {
                eprintln!("Couldn't read_exact SstIndexIter");
                self.completed = true;
                return Ok(true);
            }

            let mut block = BlockIter::new(buffer);
            block.seek(prefix)?;
            self.data_block_iter = Some(block);
            return Ok(true);
        }

        Ok(false)
    }
}

struct BlockIter {
    reader: BlockReader,
    prev_entry_size: u64,
}

impl BlockIter {
    pub fn new(block: Vec<u8>) -> Self {
        Self { reader: BlockReader::new(block), prev_entry_size: 0 }
    }
}

impl KvIterator for BlockIter {
    fn next(&mut self) -> Option<InternalKVPair> {
        let reader = &mut self.reader;
        if let Ok(entry) = bincode::deserialize_from(reader) {
            self.prev_entry_size = bincode::serialized_size(&entry).unwrap();
            Some(entry)
        } else {
            None
        }
    }

    fn seek(&mut self, prefix: &[u8]) -> Result<bool> {
        let mut iter_count: u64 = 0;

        while let Some((k, _)) = self.next() {
            iter_count += 1;
            if prefix_cmp(k.user_key.as_slice(), prefix).is_ge() {
                let size = self.prev_entry_size as usize;
                self.reader.cursor -= size;
                iter_count -= 1;
                return Ok(iter_count != 0);
            }
        }

        Ok(iter_count != 0)
    }
}

struct BlockReader {
    pub cursor: usize,
    pub buffer: Vec<u8>,
}

impl BlockReader {
    pub fn new(block: Vec<u8>) -> Self {
        BlockReader { cursor: 0, buffer: block }
    }
}

impl Read for BlockReader {
    fn read(&mut self, mut buf: &mut [u8]) -> std::io::Result<usize> {
        if self.cursor == self.buffer.len() {
            return Ok(0);
        }

        let to_copy = &self.buffer[self.cursor..];
        let nwritten = buf.write(to_copy)?;
        self.cursor += nwritten;

        Ok(nwritten)
    }
}

pub struct MemTableIterator<'a> {
    front: InternalKey,
    back: InternalKey,
    completed: bool,
    iter: Peekable<Iter<'a, InternalKey, Option<Vec<u8>>>>,
}

impl<'a> KvIterator for MemTableIterator<'a> {
    fn next(&mut self) -> Option<InternalKVPair> {
        if self.completed {
            None
        } else {
            let entry = self.iter.next()?;
            Some((entry.key().clone(), entry.value().clone()))
        }
    }

    fn seek(&mut self, prefix: &[u8]) -> Result<bool> {
        // check that prefix is within [front, back]
        if prefix_cmp(self.back.user_key.as_slice(), prefix).is_lt()
            || prefix_cmp(self.front.user_key.as_slice(), prefix).is_gt()
        {
            self.completed = true;
            return Ok(true);
        }

        let mut pos_moved = false;
        while self
            .iter
            .next_if(|e| {
                prefix_cmp(e.key().user_key.as_slice(), prefix).is_lt()
            })
            .is_some()
        {
            pos_moved = true;
        }

        Ok(pos_moved)
    }
}

pub fn memtable_kv_iterator(memtable: &MemTable) -> MemTableIterator {
    let front = memtable.front().unwrap().key().clone();
    let back = memtable.back().unwrap().key().clone();
    MemTableIterator {
        iter: memtable.iter().peekable(),
        front,
        back,
        completed: false,
    }
}

#[inline]
fn prefix_cmp(key: &[u8], prefix: &[u8]) -> Ordering {
    if key.starts_with(prefix) { Ordering::Equal } else { key.cmp(prefix) }
}
