use crate::error::Result;
use crate::iterator::KvIterator;
use crate::InternalKey;

pub(crate) struct DedupeIterator<T: KvIterator> {
    prev_key: Option<InternalKey>,
    inner: T,
    completed: bool,
}

impl<T: KvIterator> DedupeIterator<T> {
    pub fn new(from: T) -> Self {
        DedupeIterator { prev_key: None, inner: from, completed: false }
    }
}

impl<T: KvIterator> KvIterator for DedupeIterator<T> {
    fn next(&mut self) -> Option<(InternalKey, Option<Vec<u8>>)> {
        if self.completed {
            return None;
        }

        match (&self.prev_key, self.inner.next()) {
            (_, None) => {
                self.completed = true;
                None
            }
            (Some(pk), Some((k, v))) => {
                if pk.user_key != k.user_key {
                    self.prev_key = Some(k.clone());
                    Some((k, v))
                } else {
                    while let Some((k, v)) = self.inner.next() {
                        if pk.user_key != k.user_key {
                            self.prev_key = Some(k.clone());
                            return Some((k, v));
                        }
                    }

                    self.completed = true;
                    None
                }
            }
            (None, Some((k, v))) => {
                self.prev_key = Some(k.clone());
                Some((k, v))
            }
        }
    }

    fn seek(&mut self, prefix: &[u8]) -> Result<bool> {
        self.inner.seek(prefix)
    }
}
