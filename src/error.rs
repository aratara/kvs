use std::str::Utf8Error;
use std::sync::Arc;

pub type Result<T> = std::result::Result<T, KvError>;

#[derive(Debug, Clone)]
pub enum KvError {
    KeyNotFound,
    UnexpectedEntry,
    Io(Arc<std::io::Error>),
    Deserialize(Utf8Error),
    Serde(Arc<serde_json::Error>),
    UnexpectedDatabaseVersion(u64),
    Bincode(Arc<bincode::Error>),
    CorruptSst,
}

impl From<std::io::Error> for KvError {
    fn from(err: std::io::Error) -> KvError {
        KvError::Io(Arc::new(err))
    }
}

impl From<serde_json::Error> for KvError {
    fn from(err: serde_json::Error) -> KvError {
        KvError::Serde(Arc::new(err))
    }
}

impl From<Utf8Error> for KvError {
    fn from(err: Utf8Error) -> KvError {
        KvError::Deserialize(err)
    }
}

impl From<bincode::Error> for KvError {
    fn from(err: bincode::Error) -> KvError {
        KvError::Bincode(Arc::new(err))
    }
}
