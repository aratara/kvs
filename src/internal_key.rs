use serde::{Deserialize, Serialize};
use std::{
    cmp::{Ord, Ordering},
    fmt::{Display, Formatter},
};

#[derive(Clone, Debug, Eq, Serialize, Deserialize)]
pub struct InternalKey {
    pub user_key: Vec<u8>,
    pub seq: u64,
}

impl Display for InternalKey {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}, {}", self.user_key, self.seq)
    }
}

impl Ord for InternalKey {
    fn cmp(&self, other: &Self) -> Ordering {
        if self.user_key == other.user_key {
            match self.seq.cmp(&other.seq) {
                Ordering::Equal => Ordering::Equal,
                Ordering::Greater => Ordering::Less,
                Ordering::Less => Ordering::Greater,
            }
        } else {
            self.user_key.cmp(&other.user_key)
        }
    }
}

impl PartialOrd for InternalKey {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for InternalKey {
    fn eq(&self, other: &Self) -> bool {
        self.cmp(other) == Ordering::Equal
    }
}
