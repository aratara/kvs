#[derive(Copy, Clone)]
pub enum FsyncOptions {
    None,
    AfterEachWrite,
    AfterBytesWritten(u64),
}

#[derive(Copy, Clone)]
pub struct DbOptions {
    pub fsync_options: FsyncOptions,
    // The minimum size of a data block to use when flushing immtable to an
    // SST
    pub min_data_block_size: u64,
    pub max_wal_file_size: u64,
    pub min_sst_num_for_compaction: usize,
    pub min_immtable_num_for_flush: usize,
}

impl Default for DbOptions {
    fn default() -> Self {
        DbOptions {
            min_data_block_size: 4 * 1024,
            max_wal_file_size: 1024 * 1024,
            fsync_options: FsyncOptions::AfterEachWrite,
            min_sst_num_for_compaction: 4,
            min_immtable_num_for_flush: 2,
        }
    }
}
