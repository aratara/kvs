use kvs::{DbOptions, FsyncOptions, KvStore, Result};
use std::env::current_dir;

const DB_OPTIONS: DbOptions = DbOptions {
    min_data_block_size: 4 * 1024,
    max_wal_file_size: 1024 * 1024,
    fsync_options: FsyncOptions::None,
    min_sst_num_for_compaction: 4,
    min_immtable_num_for_flush: 2,
};

fn main() -> Result<()> {
    let dir = current_dir()?;
    let store = KvStore::open(&dir, Some(DB_OPTIONS))?;

    store.set(b"name", b"aratara")?;
    let name = store.get(b"name".as_slice())?.unwrap();
    let name = std::str::from_utf8(&name)?;

    println!("Name: {name}");

    Ok(())
}
