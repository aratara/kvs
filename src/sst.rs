use crate::*;
use std::{
    collections::BTreeMap,
    fs::{File, OpenOptions},
    io::{BufReader, BufWriter, Seek, SeekFrom, Write},
    path::PathBuf,
};

#[derive(Debug)]
pub struct Sst {
    pub path: PathBuf,
    pub file: File,
    pub index: DataBlockIndex,
}

impl Sst {
    pub fn load(path: PathBuf) -> Result<Self> {
        let file = File::open(path.clone())?;
        let mut reader = BufReader::new(file);

        reader.seek(SeekFrom::End(-16))?;
        let index_offset: u64 = bincode::deserialize_from(&mut reader)?;
        let magic: [u8; 8] = bincode::deserialize_from(&mut reader)?;
        if &magic != SST_MAGIC {
            return Err(KvError::CorruptSst);
        }

        reader.seek(SeekFrom::Start(index_offset))?;
        let data_block_index: DataBlockIndex =
            bincode::deserialize_from(&mut reader)?;

        Ok(Sst { path, file: reader.into_inner(), index: data_block_index })
    }
}

pub fn flush_table_as_sst(
    table: &MemTable,
    fpath: PathBuf,
    min_block_size: u64,
) -> Result<Sst> {
    let sst_file = OpenOptions::new()
        .create(true)
        .write(true)
        .truncate(true)
        .open(fpath.as_path())?;

    let mut writer = BufWriter::new(sst_file);
    let mut offset = 0;
    let mut block_size: u64 = 0;
    let mut data_block_start_key: Option<InternalKey> = None;
    let mut data_block_index = BTreeMap::<InternalKey, (u64, u64)>::new();

    for entry in table.into_iter() {
        let key = entry.key().clone();
        let value = entry.value();

        // TODO: serialized_size vs writer.stream_position
        let entry = (key, value);
        let size = bincode::serialized_size(&entry)?;
        bincode::serialize_into(&mut writer, &entry)?;

        block_size += size;
        if data_block_start_key.is_none() {
            data_block_start_key = Some(entry.0);
        }

        if block_size >= min_block_size {
            data_block_index
                .insert(data_block_start_key.unwrap(), (offset, block_size));
            offset += block_size;
            block_size = 0;
            data_block_start_key = None;
        }
    }

    if let Some(key) = data_block_start_key {
        data_block_index.insert(key, (offset, block_size));
    }
    offset += block_size;

    let data_block_index: Vec<(InternalKey, (u64, u64))> =
        data_block_index.into_iter().collect();

    let index_offset = offset;
    bincode::serialize_into(&mut writer, &data_block_index)?;
    bincode::serialize_into(&mut writer, &index_offset)?;
    bincode::serialize_into(&mut writer, SST_MAGIC)?;
    writer.flush()?;
    writer.get_ref().sync_all()?;

    let reader = File::open(fpath.as_path())?;
    Ok(Sst { path: fpath, file: reader, index: data_block_index })
}
