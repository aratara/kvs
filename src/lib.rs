#![feature(box_into_inner)]
#![feature(map_first_last)]
#![allow(clippy::needless_return)]

mod config;
pub use config::{DbOptions, FsyncOptions};

mod error;
pub use error::{KvError, Result};

mod internal_key;
use internal_key::InternalKey;

mod iterator;
pub use iterator::KvIterator;

mod prefix_iterator;
pub use prefix_iterator::PrefixIterator;

mod dedupe_iterator;

mod sst;

mod kv;
pub use kv::KvStore;

const SST_MAGIC: &[u8; 8] = b"SSTMAGIC";
const DB_VERSION: u64 = 1;

type MemTable = crossbeam_skiplist::SkipMap<InternalKey, Option<Vec<u8>>>;
type DataBlockIndex = Vec<(InternalKey, (u64, u64))>; // (offset, length)
type InternalKVPair = (InternalKey, Option<Vec<u8>>);
