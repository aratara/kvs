use crate::{
    config::{DbOptions, FsyncOptions},
    dedupe_iterator::DedupeIterator,
    error::{KvError, Result},
    internal_key::InternalKey,
    iterator::{memtable_kv_iterator, KvIterator, MergingIterator, SstIter},
    sst::{flush_table_as_sst, Sst},
    MemTable, PrefixIterator, DB_VERSION,
};
use crossbeam_skiplist::SkipMap;
use serde::{Deserialize, Serialize};
use std::{
    cell::UnsafeCell,
    collections::{HashSet, VecDeque},
    fs::{File, OpenOptions},
    io::{BufReader, BufWriter, Seek, SeekFrom, Write},
    mem::ManuallyDrop,
    os::unix::prelude::{FromRawFd, IntoRawFd},
    path::{Path, PathBuf},
    sync::{
        atomic::{AtomicU64, Ordering},
        mpsc::{sync_channel, SyncSender},
        Arc, Condvar, Mutex, MutexGuard, RwLock, RwLockWriteGuard,
    },
    thread::{self, JoinHandle},
};

// This gets persisted onto disk so we know which files are part of our
// database.
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct DbVersion {
    // Version of the database code
    db_version: u64,
    // Last persisted sequence number
    last_written_seq: u64,
    // generation corresponding to the mutable memtable
    current_gen: u64,
    // generations corresponding to immutable memtable
    imm_gens: Vec<u64>,
    // generations corresponding to SSTs
    sst_gens: Vec<u64>,

    // file descriptor for this version file
    // i'm using a raw file descriptor so that I can share the file
    // across multiple threads. `DbVersion` will be protected by a Mutex so we
    // are safe.
    #[serde(skip)]
    fd: i32,
}

impl DbVersion {
    fn new(file_path: PathBuf) -> Result<Self> {
        let file = OpenOptions::new()
            .create(true)
            .write(true)
            .open(file_path.as_path())?;

        let version = DbVersion {
            db_version: DB_VERSION,
            last_written_seq: 0,
            current_gen: 1,
            imm_gens: Vec::new(),
            sst_gens: vec![],
            fd: file.into_raw_fd(),
        };

        version.write_to_file_sync(true)?;

        Ok(version)
    }

    fn load_from_path(file_path: PathBuf) -> Result<Self> {
        let version_file =
            OpenOptions::new().read(true).open(file_path.as_path())?;
        let version_reader = BufReader::new(version_file);
        let mut version: DbVersion = serde_json::from_reader(version_reader)?;

        version.fd = OpenOptions::new()
            .write(true)
            .open(file_path.as_path())?
            .into_raw_fd();

        Ok(version)
    }

    fn write_to_file_sync(&self, fsync: bool) -> Result<()> {
        let mut file = ManuallyDrop::new(unsafe { File::from_raw_fd(self.fd) });
        file.set_len(0)?;
        file.seek(SeekFrom::Start(0))?;
        let mut writer = BufWriter::new(file.by_ref());
        serde_json::to_writer_pretty(&mut writer, self)?;

        if fsync {
            writer.get_mut().sync_all()?;
        }

        Ok(())
    }
}

#[derive(Debug)]
struct Flushed {
    // the immtable gen that has been flushed
    src: u64,
    dst: u64,
    sst: Sst,
}
#[derive(Debug)]
struct Compacted {
    // the sst gens that have been compacted
    src: Vec<u64>,
    dst: u64,
    sst: Sst,
}
#[derive(Debug)]
struct VersionEdit {
    flushed: VecDeque<Flushed>,
    compacted: VecDeque<Compacted>,
}

impl VersionEdit {
    fn new() -> Self {
        Self { flushed: VecDeque::default(), compacted: VecDeque::default() }
    }
}

#[derive(Serialize, Deserialize, Debug)]
// Entries that will be written to Write Ahead Log (WAL)
pub enum LogEntry {
    Set { key: InternalKey, value: Vec<u8> },
    Rm { key: InternalKey },
}
impl LogEntry {
    fn key(&self) -> &InternalKey {
        match self {
            Self::Set { key, .. } => key,
            Self::Rm { key } => key,
        }
    }
}

enum DbState {
    Pending { prev_batch_notif: Arc<DbStateNotify> },
    PendingLeader { writes: Vec<LogEntry>, batch_notify: Arc<DbStateNotify> },
}
impl DbState {
    fn new() -> Self {
        DbState::Pending {
            prev_batch_notif: Arc::new(DbStateNotify::new(
                WriteState::Completed,
            )),
        }
    }
}

enum WriteState {
    Completed,
    Pending,
    Error(KvError),
}

struct DbStateNotify(Mutex<WriteState>, Condvar);
impl DbStateNotify {
    fn new(val: WriteState) -> Self {
        Self(Mutex::new(val), Condvar::new())
    }

    fn wait(&self) -> Result<()> {
        let mut started = self.0.lock().unwrap();
        loop {
            match *started {
                WriteState::Pending => {
                    started = self.1.wait(started).unwrap();
                }
                WriteState::Completed => {
                    return Ok(());
                }
                WriteState::Error(ref e) => {
                    return Err(KvError::clone(e));
                }
            }
        }
    }
}

struct VersionMetadata {
    dir_path: PathBuf,
    latest: Mutex<DbVersion>,

    // SAFETY: Only one thread will be using this at a time (all writes happen
    // on the leader thread).
    writer: UnsafeCell<BufWriter<File>>,
    num_written: UnsafeCell<u64>,

    // generation used when creating a new WAL/SST file
    next_gen: AtomicU64,

    // Used for keeping track of sst/memtable generations still in use (by
    // iterators) so that they don't get garbage collected.
    referenced_versions: SkipMap<u64, Arc<DbVersion>>,
    version_id: AtomicU64,

    // Only used by the background thread when flushing/compacting
    pending_edits: Mutex<VersionEdit>,
}

impl VersionMetadata {
    fn new(version: DbVersion, dir_path: PathBuf) -> Result<Self> {
        let current_gen = version.current_gen;
        let log_path =
            make_db_file_path(version.current_gen, dir_path.clone(), None);
        let mut memtable_log_file = OpenOptions::new()
            .create(true)
            .append(true)
            .open(log_path.as_path())?;
        memtable_log_file.seek(SeekFrom::End(0))?;

        let writer = UnsafeCell::new(BufWriter::new(memtable_log_file));

        let mut gens: Vec<u64> = vec![current_gen];
        version.imm_gens.iter().for_each(|x| gens.push(*x));
        version.sst_gens.iter().for_each(|x| gens.push(*x));
        let next_gen = gens.iter().max().unwrap_or(&1) + 1;

        let immutable_versions = SkipMap::default();
        immutable_versions.insert(0, Arc::new(version.clone()));

        Ok(VersionMetadata {
            dir_path,
            latest: Mutex::new(version),
            num_written: UnsafeCell::new(0),
            next_gen: AtomicU64::new(next_gen),
            writer,
            referenced_versions: immutable_versions,
            version_id: AtomicU64::new(1),
            pending_edits: Mutex::new(VersionEdit::new()),
        })
    }

    fn next_db_file_path(&self, extension: Option<&str>) -> (u64, PathBuf) {
        let generation = self.next_gen.fetch_add(1, Ordering::Relaxed);
        let extension = extension.unwrap_or("wal");
        (
            generation,
            self.dir_path.clone().join(format!("{}.{}", generation, extension)),
        )
    }

    // This is a bit awkward, but I don't want this function to be called
    // with a random DbVersion, hence the MutexGuard.
    // Maybe we can return a special guard struct when locking `latest` and when
    // that gets `drop`ped, the version gets appended to the list automatically?
    fn version_did_change<'a>(
        &self,
        version: MutexGuard<'a, DbVersion>,
    ) -> MutexGuard<'a, DbVersion> {
        let id = self.version_id.fetch_add(1, Ordering::Relaxed);
        self.referenced_versions.insert(id, Arc::new((*version).clone()));
        version
    }
}

// Commands to send to the background worker thread
enum BgCommand {
    Exit,
    // garbage collect/flush immtable/compaction
    DoWork,
}

// TODO: Feels weird that I'm Arc-ing everything... Maybe I can wrap the
// KvStore in an Arc instead but will need to figure out a way for the bg_thread
// to also have access somehow.
pub struct KvStore {
    dir_path: Arc<PathBuf>,
    metadata: Arc<VersionMetadata>,
    state: Arc<Mutex<DbState>>,

    // The memtables are `Arc`ed because they can be shifted from `memtable`
    // into `imm_tables` while iterators are still referencing them.
    //
    // The `memtable` will be shifted into `imm_tables` when the WAL
    // file exceeds the configured size. A write lock is used when shifting the
    // memtable. A read lock can be used to view/update the MemTable which
    // is a lock-free data structure.
    //
    // TODO: Can we use a ring buffer kind of thing instead, and pass the indices
    //       instead of allocating on the heap? We can maybe re-use memtables
    //       instead of creating new ones all the time too.
    memtable: Arc<RwLock<Arc<MemTable>>>,
    imm_tables: Arc<SkipMap<u64, Arc<MemTable>>>,
    sst_map: Arc<SkipMap<u64, Sst>>,
    next_seq: Arc<AtomicU64>,
    visible_seq: Arc<AtomicU64>,

    // Used to communicate with background thread which is in charge of
    // flushing, compaction, garbage collection
    bg_tx: Arc<SyncSender<BgCommand>>,
    bg_thread: Arc<Mutex<Option<JoinHandle<()>>>>,

    options: Arc<DbOptions>,
}

unsafe impl Send for KvStore {}
unsafe impl Sync for KvStore {}

impl Clone for KvStore {
    fn clone(&self) -> Self {
        KvStore {
            dir_path: Arc::clone(&self.dir_path),
            metadata: Arc::clone(&self.metadata),
            state: Arc::clone(&self.state),
            memtable: Arc::clone(&self.memtable),
            imm_tables: Arc::clone(&self.imm_tables),
            sst_map: Arc::clone(&self.sst_map),
            next_seq: Arc::clone(&self.next_seq),
            visible_seq: Arc::clone(&self.visible_seq),
            bg_tx: Arc::clone(&self.bg_tx),
            bg_thread: Arc::clone(&self.bg_thread),
            options: Arc::clone(&self.options),
        }
    }
}

impl KvStore {
    pub fn open(
        path: impl Into<PathBuf>,
        options: Option<DbOptions>,
    ) -> Result<Self> {
        let path: PathBuf = path.into();

        // get/create database version file
        let version_path = version_file_path(path.clone());
        let version = if version_path.exists() {
            DbVersion::load_from_path(version_path)?
        } else {
            DbVersion::new(version_path)?
        };
        if version.db_version != DB_VERSION {
            return Err(KvError::UnexpectedDatabaseVersion(version.db_version));
        }

        // load immtables
        let imm_tables = SkipMap::new();
        for gen in version.imm_gens.iter() {
            let log_path = make_db_file_path(*gen, path.clone(), None);
            let memtable = load_memtable(log_path.as_path())?;
            imm_tables.insert(*gen, Arc::new(memtable));
        }

        // load sst files
        let sst_map: SkipMap<u64, Sst> = SkipMap::new();
        for gen in version.sst_gens.iter() {
            let sst_path = make_db_file_path(*gen, path.clone(), Some("sst"));
            let sst = Sst::load(sst_path)?;
            sst_map.insert(*gen, sst);
        }

        // load memtable
        let log_path =
            make_db_file_path(version.current_gen, path.clone(), None);
        let mut memtable_log_file = OpenOptions::new()
            .create(true)
            .append(true)
            .open(log_path.as_path())?;
        memtable_log_file.seek(SeekFrom::End(0))?;
        let memtable = load_memtable(log_path.as_path())?;

        let last_written_seq = version.last_written_seq;
        let version_md = VersionMetadata::new(version, path.clone())?;
        let (sender, receiver) = sync_channel(1);

        let db = KvStore {
            metadata: Arc::new(version_md),
            state: Arc::new(Mutex::new(DbState::new())),
            memtable: Arc::new(RwLock::new(Arc::new(memtable))),
            imm_tables: Arc::new(imm_tables),
            sst_map: Arc::new(sst_map),
            dir_path: Arc::new(path),
            next_seq: Arc::new(AtomicU64::new(last_written_seq + 1)),
            visible_seq: Arc::new(AtomicU64::new(last_written_seq)),
            bg_tx: Arc::new(sender),
            bg_thread: Arc::new(Mutex::new(None)),
            options: Arc::new(options.unwrap_or_default()),
        };

        // setup background worker thread for garbage collecting versions,
        // flushing tables, compacting ssts
        let cloned = db.clone();
        let bg_thread = thread::spawn(move || {
            let db = cloned;
            while let BgCommand::DoWork = receiver.recv().unwrap() {
                for entry in
                    db.metadata.referenced_versions.iter().rev().skip(1)
                {
                    let version = entry.value();
                    if Arc::strong_count(version) == 1 {
                        db.metadata.referenced_versions.remove(entry.key());
                    }
                }

                if let Err(e) = db.try_flush_immtable() {
                    eprintln!("{e:?}");
                }
                if let Err(e) = db.try_compact() {
                    eprintln!("{e:?}");
                }

                db.try_apply_edits();
            }

            return;
        });

        *db.bg_thread.lock().unwrap() = Some(bg_thread);

        Ok(db)
    }

    pub fn set(&self, key: &[u8], value: &[u8]) -> Result<()> {
        let seq = self.next_seq.fetch_add(1, Ordering::SeqCst);
        let key = InternalKey { user_key: key.to_vec(), seq };
        let entry = LogEntry::Set { key, value: value.to_vec() };

        self.write_entry(entry)
    }

    pub fn get(&self, key: &[u8]) -> Result<Option<Vec<u8>>> {
        let visible_seq = self.visible_seq.load(Ordering::Relaxed);
        let mut iter = self.full_iterator(Some(visible_seq));
        iter.seek(key).expect("Seek on KvIterator should work...!");

        while let Some((k, v)) = iter.next() {
            if k.user_key.as_slice() < key {
                continue;
            }
            if k.user_key.as_slice() > key {
                return Ok(None);
            }

            return Ok(v);
        }

        Ok(None)
    }

    pub fn remove(&self, key: &[u8]) -> Result<()> {
        let seq = self.next_seq.fetch_add(1, Ordering::Relaxed);
        let key = InternalKey { user_key: key.to_vec(), seq };
        let entry = LogEntry::Rm { key };

        self.write_entry(entry)
    }

    pub fn db_iterator(
        &self,
        prefixed: Option<&[u8]>,
    ) -> Result<Box<dyn KvIterator + '_>> {
        let visible_seq = self.visible_seq.load(Ordering::SeqCst);
        let full_iter = self.full_iterator(Some(visible_seq));

        match prefixed {
            None => {
                return Ok(Box::new(DedupeIterator::new(full_iter)));
            }
            Some(prefix) => {
                let prefix_iter = PrefixIterator::new(full_iter, prefix)?;
                return Ok(Box::new(DedupeIterator::new(prefix_iter)));
            }
        }
    }

    pub fn full_iterator(&self, max_seq: Option<u64>) -> impl KvIterator + '_ {
        let memtable = self.memtable.read().unwrap();
        let version = self.metadata.referenced_versions.back().unwrap();
        let version = version.value();

        let mut result =
            MergingIterator::new(max_seq, Arc::clone(version), || {
                let _ = self.bg_tx.try_send(BgCommand::DoWork);
            });

        let memtable = Arc::as_ptr(&memtable);
        // SAFETY: MemTable/ImmTable pointers won't be invalid as long as we
        // don't GC the tables. GC only happens when there are no more
        // references to the table so we are fine.
        // ImmTable iterators won't change because the table is immutable.
        // MemTable iterators might change but the values we are interested in
        // won't because every new entry will have a different sequence number.
        unsafe {
            result.add_iter(memtable_kv_iterator(&*memtable));
        }

        for entry in self.imm_tables.iter() {
            let table = entry.value();
            let table = Arc::as_ptr(table);
            unsafe {
                result.add_iter(memtable_kv_iterator(&*table));
            }
        }

        for entry in self.sst_map.iter() {
            let sst = entry.value();
            result.add_iter(SstIter::new(sst));
        }

        result
    }

    // adapted from http://justinjaffray.com/durability-and-redo-logging/
    fn write_entry(&self, entry: LogEntry) -> Result<()> {
        let mut state = self.state.lock().unwrap();

        let write_entries_to_disk = |entries: &[LogEntry]| -> Result<u64> {
            let mut writer = unsafe { &mut *self.metadata.writer.get() };
            let mut start_position = writer.stream_position()?;

            if start_position >= self.options.max_wal_file_size {
                let memtable_guard = self.memtable.write().unwrap();
                writer.flush()?;
                writer.get_mut().sync_all()?;
                writer = self.shift_memtable(memtable_guard)?;
                start_position = 0;
            }

            let mut version = self.metadata.latest.lock().unwrap();
            let mut last_written_seq = entries[0].key().seq;
            let mut buffer: Vec<u8> = vec![];

            for entry in entries.iter() {
                bincode::serialize_into(&mut buffer, &entry)?;
                last_written_seq = entry.key().seq.max(last_written_seq);
            }

            version.last_written_seq = last_written_seq;
            writer.write_all(&buffer)?;
            writer.flush()?;

            match self.options.fsync_options {
                FsyncOptions::AfterEachWrite => {
                    writer.get_mut().sync_all()?;
                    version.write_to_file_sync(true)?;
                }
                FsyncOptions::AfterBytesWritten(min_bytes) => {
                    let end_position = writer.stream_position()?;
                    let total_written = end_position - start_position;
                    let num_written =
                        unsafe { &mut *self.metadata.num_written.get() };
                    *num_written += total_written;

                    if *num_written >= min_bytes {
                        writer.get_mut().sync_all()?;
                        version.write_to_file_sync(true)?;
                        *num_written = 0;
                    }
                }
                FsyncOptions::None => {
                    version.write_to_file_sync(false)?;
                }
            }
            drop(version);

            Ok(last_written_seq)
        };

        match &mut *state {
            DbState::Pending { .. } => {
                let done = Arc::new(DbStateNotify::new(WriteState::Pending));

                let prev_notif = if let DbState::Pending { prev_batch_notif } =
                    std::mem::replace(
                        &mut *state,
                        DbState::PendingLeader {
                            writes: vec![entry],
                            batch_notify: Arc::clone(&done),
                        },
                    ) {
                    prev_batch_notif
                } else {
                    panic!("invalid state")
                };
                drop(state);

                // wait for previous batch to finish writing
                //
                // should I care if the previous batch errored?
                // maybe there is some "cleaning up" to do?
                let _ = prev_notif.wait();

                let mut state = self.state.lock().unwrap();
                let writes = if let DbState::PendingLeader { writes, .. } =
                    std::mem::replace(
                        &mut *state,
                        DbState::Pending {
                            prev_batch_notif: Arc::clone(&done),
                        },
                    ) {
                    writes
                } else {
                    panic!("expected to still be the leader");
                };
                drop(state);

                match write_entries_to_disk(&writes) {
                    Ok(last_written_seq) => {
                        let memtable = self.memtable.read().unwrap();
                        for entry in writes {
                            match entry {
                                LogEntry::Rm { key } => {
                                    memtable.insert(key, None);
                                }
                                LogEntry::Set { key, value } => {
                                    memtable.insert(key, Some(value));
                                }
                            }
                        }

                        // now that we've written everything to disk and
                        // inserted it into our memtable, we can let
                        // readers know about the new entries by updating
                        // `visible_seq`
                        self.visible_seq
                            .store(last_written_seq, Ordering::SeqCst);
                        *done.0.lock().unwrap() = WriteState::Completed;
                        done.1.notify_all();
                        return Ok(());
                    }

                    Err(e) => {
                        eprintln!("Write Error: {:?}", e);
                        *done.0.lock().unwrap() = WriteState::Error(e.clone());
                        done.1.notify_all();
                        return Err(e);
                    }
                }
            }

            DbState::PendingLeader { writes, batch_notify } => {
                writes.push(entry);
                let notify = Arc::clone(batch_notify);
                drop(state);
                return notify.wait();
            }
        }
    }

    // Shifts memtable into imm_table list, assigns new memtable and
    // corresponding WAL writer.
    // Version is updated but NOT written to disk.
    // Returns BufWriter for the newly created WAL file.
    fn shift_memtable(
        &self,
        guard: RwLockWriteGuard<Arc<MemTable>>,
    ) -> Result<&mut BufWriter<File>> {
        let mut version = self.metadata.latest.lock().unwrap();
        let current_gen = version.current_gen;

        let mut memtable = guard;
        let old_table = std::mem::take(&mut *memtable);
        self.imm_tables.insert(current_gen, old_table);
        version.imm_gens.push(current_gen);

        let (new_gen, wal_path) = self.metadata.next_db_file_path(None);
        version.current_gen = new_gen;
        let _ignore = self.metadata.version_did_change(version);

        let mut new_writer = OpenOptions::new()
            .create(true)
            .append(true)
            .open(wal_path.as_path())?;
        new_writer.seek(SeekFrom::End(0))?;

        unsafe { *self.metadata.writer.get() = BufWriter::new(new_writer) };
        let _ = self.bg_tx.try_send(BgCommand::DoWork);

        let writer = unsafe { &mut *self.metadata.writer.get() };

        Ok(writer)
    }

    fn try_flush_immtable(&self) -> Result<()> {
        let mut pending_edits = self.metadata.pending_edits.lock().unwrap();
        let entry = self.metadata.referenced_versions.back().unwrap();
        let version = entry.value();

        let already_flushed: HashSet<u64> =
            pending_edits.flushed.iter().map(|x| x.src).collect();

        let all_imm_tables: HashSet<u64> =
            version.imm_gens.iter().copied().collect();

        let diff = all_imm_tables.difference(&already_flushed);
        let count = diff.clone().count();

        if count >= self.options.min_immtable_num_for_flush {
            let gen = diff.min().unwrap();
            let table = self.imm_tables.get(gen).unwrap();
            let table = table.value();
            let (sst_gen, file_path) =
                self.metadata.next_db_file_path(Some("sst"));
            let sst = flush_table_as_sst(
                table,
                file_path,
                self.options.min_data_block_size,
            )?;

            pending_edits.flushed.push_back(Flushed {
                src: *gen,
                dst: sst_gen,
                sst,
            });
        }

        Ok(())
    }

    fn try_compact(&self) -> Result<()> {
        let mut pending_edits = self.metadata.pending_edits.lock().unwrap();
        if !pending_edits.compacted.is_empty() {
            return Ok(());
        }

        let version = self.metadata.referenced_versions.back().unwrap();
        let version = version.value();
        let ssts = &version.sst_gens;
        let visible_seq = self.visible_seq.load(Ordering::Relaxed);

        if ssts.len() < self.options.min_sst_num_for_compaction {
            return Ok(());
        }

        let mut iterator =
            MergingIterator::new(Some(visible_seq), Arc::clone(version), || {});

        for sst_gen in ssts {
            let entry = self.sst_map.get(sst_gen).unwrap();
            let sst = entry.value();
            iterator.add_iter(SstIter::new(sst));
        }

        let merged = MemTable::new();
        while let Some((k, v)) = iterator.next() {
            // We need to make sure to delete old versions of a given key.
            //
            // If there are multiple versions of a key, the iterator will
            // emit the entry with the highest sequence number first.
            // So if the table already contains an entry, it should be the most
            // up to date version
            if !merged.contains_key(&k) {
                merged.insert(k, v);
            }
        }

        let (compaction_gen, file_path) =
            self.metadata.next_db_file_path(Some("sst"));
        let sst = flush_table_as_sst(
            &merged,
            file_path,
            self.options.min_data_block_size,
        )?;

        pending_edits.compacted.push_back(Compacted {
            src: ssts.clone(),
            dst: compaction_gen,
            sst,
        });

        Ok(())
    }

    fn try_apply_edits(&self) {
        // Find all the imm/sst gens that are still being referenced
        // If there are VersionEdits whose source generations are no longer
        // referenced, then we are allowed to apply the edit

        let mut pending_edits = self.metadata.pending_edits.lock().unwrap();
        let mut referenced_gens: HashSet<u64> = HashSet::default();
        for entry in self.metadata.referenced_versions.iter().rev().skip(1) {
            let version = entry.value();
            referenced_gens.insert(version.current_gen);
            version.imm_gens.iter().for_each(|x| {
                referenced_gens.insert(*x);
            });
            version.sst_gens.iter().for_each(|x| {
                referenced_gens.insert(*x);
            });
        }

        let mut wals_to_delete = vec![];
        let mut ssts_to_delete = vec![];

        let table_guard = self.memtable.write().unwrap();
        let mut version = self.metadata.latest.lock().unwrap();

        let mut flush_later: VecDeque<Flushed> = VecDeque::default();
        while let Some(Flushed { src, dst, sst }) =
            pending_edits.flushed.pop_front()
        {
            if !referenced_gens.contains(&src) {
                self.imm_tables.remove(&src);
                self.sst_map.insert(dst, sst);

                let idx =
                    version.imm_gens.iter().position(|x| x == &src).unwrap();
                version.imm_gens.remove(idx);
                version.sst_gens.push(dst);
                wals_to_delete.push(src);
            } else {
                flush_later.push_back(Flushed { src, dst, sst });
            }
        }
        pending_edits.flushed.append(&mut flush_later);

        let mut compact_later: VecDeque<Compacted> = VecDeque::default();
        while let Some(Compacted { src, dst, sst }) =
            pending_edits.compacted.pop_front()
        {
            if !src.iter().any(|x| referenced_gens.contains(x)) {
                for gen in src.iter() {
                    self.sst_map.remove(gen);
                    let idx =
                        version.sst_gens.iter().position(|x| x == gen).unwrap();
                    version.sst_gens.remove(idx);
                    ssts_to_delete.push(*gen);
                }
                self.sst_map.insert(dst, sst);
                version.sst_gens.push(dst);
            } else {
                compact_later.push_back(Compacted { src, dst, sst });
            }
        }
        pending_edits.compacted.append(&mut compact_later);

        let version_did_change =
            !wals_to_delete.is_empty() || !ssts_to_delete.is_empty();
        if version_did_change {
            version = self.metadata.version_did_change(version);
            if let Err(e) = version.write_to_file_sync(true) {
                eprintln!("{e:?}");
            }
        }

        drop(table_guard);
        drop(version);
        drop(pending_edits);

        for gen in wals_to_delete {
            let path = make_db_file_path(
                gen,
                self.dir_path.as_ref().clone(),
                Some("wal"),
            );
            std::fs::remove_file(path).unwrap();
        }

        for gen in ssts_to_delete {
            let path = make_db_file_path(
                gen,
                self.dir_path.as_ref().clone(),
                Some("sst"),
            );
            std::fs::remove_file(path).unwrap();
        }
    }
}

impl Drop for KvStore {
    fn drop(&mut self) {
        let mut handle_guard = self.bg_thread.lock().unwrap();
        if let Some(handle) = handle_guard.as_ref() {
            // if we are the bg thread, just return
            if handle.thread().id() == std::thread::current().id() {
                return;
            }
        }
        if Arc::strong_count(&self.bg_thread) == 2 {
            if let Some(handle) = std::mem::take(&mut *handle_guard) {
                self.bg_tx.send(BgCommand::Exit).unwrap();
                drop(handle_guard);
                handle.join().expect("bruuuuh");
            }
        }
    }
}

fn make_db_file_path(
    generation: u64,
    dir_path: PathBuf,
    custom_extension: Option<&str>,
) -> PathBuf {
    let extension = custom_extension.unwrap_or("wal");
    dir_path.join(format!("{}.{}", generation, extension))
}

fn version_file_path(dir_path: PathBuf) -> PathBuf {
    dir_path.join(format!("db.{}", "version"))
}

fn load_memtable(log_path: impl AsRef<Path>) -> Result<MemTable> {
    let file = File::open(log_path.as_ref())?;
    let memtable = MemTable::new();
    let mut reader = BufReader::new(file);

    while let Ok(entry) = bincode::deserialize_from(&mut reader) {
        match entry {
            LogEntry::Set { key, value } => memtable.insert(key, Some(value)),
            LogEntry::Rm { key } => memtable.insert(key, None),
        };
    }

    Ok(memtable)
}
