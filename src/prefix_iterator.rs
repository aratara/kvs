use crate::error::Result;
use crate::iterator::KvIterator;
use crate::InternalKey;

// TODO: how do I just expose this for testing but not to users?
pub struct PrefixIterator<T: KvIterator> {
    prefix: Vec<u8>,
    inner: T,
    completed: bool,
}

impl<T: KvIterator> PrefixIterator<T> {
    pub fn new(from: T, prefix: &[u8]) -> Result<Self> {
        let mut from = from;
        from.seek(prefix)?;
        Ok(PrefixIterator {
            inner: from,
            prefix: prefix.to_vec(),
            completed: false,
        })
    }
}

impl<T: KvIterator> KvIterator for PrefixIterator<T> {
    fn next(&mut self) -> Option<(InternalKey, Option<Vec<u8>>)> {
        if self.completed {
            return None;
        }

        if let Some((key, value)) = self.inner.next() {
            if key.user_key.starts_with(&self.prefix) {
                return Some((key, value));
            }
            println!("MISSED: {:?}", key.user_key);
        }

        self.completed = true;
        None
    }

    fn seek(&mut self, _prefix: &[u8]) -> Result<bool> {
        Ok(false)
    }
}
