use kvs::{
    DbOptions, FsyncOptions, KvIterator, KvStore, PrefixIterator, Result,
};
use std::{
    env::current_dir,
    sync::{Arc, Barrier},
    thread,
};
use tempfile::TempDir;
use walkdir::WalkDir;

const TEST_CONFIG_OPTIONS: DbOptions = DbOptions {
    min_data_block_size: 4 * 1024,
    max_wal_file_size: 1024 * 1024,
    fsync_options: FsyncOptions::None,
    min_sst_num_for_compaction: 4,
    min_immtable_num_for_flush: 2,
};

fn temp_db_opener() -> Box<dyn Fn() -> KvStore> {
    let temp_dir =
        TempDir::new().expect("unable to create temporary working directory");
    Box::new(move || {
        KvStore::open(temp_dir.path(), Some(TEST_CONFIG_OPTIONS)).unwrap()
    })
}

fn cwd_db_opener() -> Box<dyn Fn() -> KvStore> {
    let dir = current_dir().expect("Unable to get current directory");
    Box::new(move || KvStore::open(&dir, Some(TEST_CONFIG_OPTIONS)).unwrap())
}

// Should get previously stored value
#[test]
fn get_stored_value() -> Result<()> {
    let db_opener = temp_db_opener();
    let store = db_opener();

    store.set(&key(1), b"value1")?;
    store.set(&key(2), b"value2")?;

    assert_eq!(store.get(&key(1))?, Some("value1".to_owned().into_bytes()));
    assert_eq!(store.get(&key(2))?, Some("value2".to_owned().into_bytes()));

    drop(store);
    let store = db_opener();
    assert_eq!(store.get(&key(1))?, Some("value1".to_owned().into_bytes()));
    assert_eq!(store.get(&key(2))?, Some("value2".to_owned().into_bytes()));

    Ok(())
}

// Should get previously stored value from an SSTable
#[test]
fn get_stored_value_from_sst() -> Result<()> {
    let db_opener = temp_db_opener();
    let store = db_opener();
    let max = 16_usize;
    for key_i in 1..(1 << max) {
        store.set(&key(key_i), format!("value{}", key_i).as_bytes()).unwrap();
    }

    for key_i in 1..(1 << max) {
        assert_eq!(
            store.get(&key(key_i))?,
            Some(format!("value{}", key_i).into_bytes())
        );
    }

    drop(store);
    let store = db_opener();

    for key_i in 1..(1 << max) {
        assert_eq!(
            store.get(&key(key_i))?,
            Some(format!("value{}", key_i).into_bytes())
        );
    }

    Ok(())
}

#[test]
fn iterator_full_db_scan() -> Result<()> {
    let db_opener = temp_db_opener();
    let store = db_opener();

    let max_key: u64 = 1 << 20;

    for key_i in 1..max_key {
        store.set(&key(key_i), format!("value{}", key_i).as_bytes()).unwrap();
    }

    let mut db_iterator = store.full_iterator(None);
    for key_i in 1..max_key {
        if let Some((_, value)) = db_iterator.next() {
            match value {
                Some(v) => {
                    let expected = format!("value{}", key_i);
                    assert_eq!(v, expected.into_bytes());
                }
                None => panic!(),
            }
        } else {
            panic!("Couldn't find index {key_i}");
        }
    }
    assert!(db_iterator.next().is_none());

    drop(db_iterator);
    drop(store);

    let store = db_opener();

    let mut db_iterator = store.full_iterator(None);
    for key_i in 1..max_key {
        if let Some((_, value)) = db_iterator.next() {
            assert_eq!(value, Some(format!("value{}", key_i).into_bytes()))
        }
    }
    assert!(db_iterator.next().is_none());

    Ok(())
}

#[test]
fn iterator_find_all_values_before_given_seq() -> Result<()> {
    let db_opener = temp_db_opener();
    let store = db_opener();

    let max_key: u64 = 1 << 12;

    for key_i in 1..max_key {
        store.set(&key(key_i), format!("value{}", key_i).as_bytes()).unwrap();
    }

    let mut db_iterator = store.full_iterator(Some(1 << 11));
    let mut check_key = 1;
    while let Some((_, value)) = db_iterator.next() {
        match value {
            Some(v) => {
                let expected = format!("value{}", check_key);
                assert_eq!(v, expected.into_bytes());
                check_key += 1;
            }
            None => panic!(),
        }
    }

    assert_eq!(check_key, (1 << 11) + 1);

    drop(db_iterator);
    drop(store);
    let store = db_opener();

    let mut db_iterator = store.full_iterator(Some(1 << 11));
    let mut check_key = 1;
    while let Some((_, value)) = db_iterator.next() {
        match value {
            Some(v) => {
                let expected = format!("value{}", check_key);
                assert_eq!(v, expected.into_bytes());
                check_key += 1;
            }
            None => panic!(),
        }
    }

    assert_eq!(check_key, (1 << 11) + 1);

    Ok(())
}

#[test]
fn iterator_find_updated_values_before_given_seq() -> Result<()> {
    let db_opener = temp_db_opener();
    let store = db_opener();
    let max_key: u64 = 1 << 12;
    for value_i in 1..=max_key {
        store.set(b"key", format!("value{}", value_i).as_bytes()).unwrap();
    }

    let mut db_iterator = store.full_iterator(Some(1 << 11));
    let mut check_value = 1 << 11;
    while let Some((_, value)) = db_iterator.next() {
        match value {
            Some(v) => {
                let expected = format!("value{}", check_value);
                assert_eq!(v, expected.into_bytes());
                check_value -= 1;
            }
            None => panic!(),
        }
    }

    assert_eq!(check_value, 0);

    drop(db_iterator);
    drop(store);
    let store = db_opener();

    let mut db_iterator = store.full_iterator(Some(1 << 11));
    let mut check_value = 1 << 11;
    while let Some((_, value)) = db_iterator.next() {
        match value {
            Some(v) => {
                let expected = format!("value{}", check_value);
                assert_eq!(v, expected.into_bytes());
                check_value -= 1;
            }
            None => panic!(),
        }
    }

    assert_eq!(check_value, 0);

    Ok(())
}

#[test]
fn iterator_holding_stops_garbage_collection() -> Result<()> {
    let db_opener = temp_db_opener();
    let store = db_opener();
    let max_key: u64 = 1 << 16;

    for key_i in 1..=max_key {
        store.set(&key(key_i), b"foo").unwrap();
    }
    let mut db_iterator = store.full_iterator(Some(1 << 16));

    for key_i in 1..=max_key {
        store.set(&key(key_i), b"bar").unwrap();
    }

    for key_i in 1..=max_key {
        if let Some((k, value)) = db_iterator.next() {
            assert_eq!(k.user_key, key(key_i));
            let expected = b"foo".to_vec();
            assert_eq!(value, Some(expected));
        }
    }
    assert!(db_iterator.next().is_none());

    Ok(())
}

#[test]
fn prefix_iterating() -> Result<()> {
    let db_opener = temp_db_opener();
    let store = db_opener();

    // Let's put in keys: A::1.., B::1.., C::1..
    // And verify prefix iteration for A::, B::, C::

    let max_key = 1 << 16;
    let tester = |prefix: &str| {
        for i in 0..=max_key {
            store
                .set(
                    &key_with_prefix(prefix, i),
                    format!("{prefix}value::{}", i).as_bytes(),
                )
                .unwrap();
        }

        let iter = store.full_iterator(None);
        let mut iter = PrefixIterator::new(iter, prefix.as_bytes()).unwrap();

        for i in 0..=max_key {
            if let Some((k, value)) = iter.next() {
                let key = key_with_prefix(prefix, i);
                assert_eq!(k.user_key, key);
                let expected =
                    format!("{prefix}value::{}", i).as_bytes().to_vec();
                assert_eq!(value, Some(expected));
            } else {
                panic!("This shouldn't have happened");
            }
        }

        assert!(iter.next().is_none());
    };

    for prefix in ["A::", "B::", "C::"] {
        tester(prefix);
    }

    Ok(())
}

#[test]
fn dedupe_iterating() -> Result<()> {
    let db_opener = temp_db_opener();
    let store = db_opener();

    let num = 1 << 16;
    for i in 0..=num {
        store
            .set("key".as_bytes(), format!("value::{}", i).as_bytes())
            .unwrap();
    }

    let mut iter = store.db_iterator(None).unwrap();
    let (_key, value) = iter.next().unwrap();
    assert_eq!(Some(format!("value::{}", num).as_bytes().to_vec()), value);
    assert!(iter.next().is_none());

    Ok(())
}

// should overwrite existent value
#[test]
fn overwrite_value() -> Result<()> {
    let db_opener = temp_db_opener();
    let store = db_opener();

    store.set(b"key1", b"value1")?;
    assert_eq!(store.get(b"key1")?, Some("value1".to_owned().into_bytes()));
    store.set(b"key1", b"value2")?;
    assert_eq!(store.get(b"key1")?, Some("value2".to_owned().into_bytes()));

    drop(store);
    let store = db_opener();

    assert_eq!(store.get(b"key1")?, Some("value2".to_owned().into_bytes()));
    store.set(b"key1", b"value3")?;
    assert_eq!(store.get(b"key1")?, Some("value3".to_owned().into_bytes()));

    Ok(())
}

// Should get `None` when getting a non-existent key
#[test]
fn get_non_existent_value() -> Result<()> {
    let db_opener = temp_db_opener();
    let store = db_opener();

    store.set(b"key1", b"value1")?;
    assert_eq!(store.get(b"key2")?, None);

    drop(store);

    let store = db_opener();
    assert_eq!(store.get(b"key2")?, None);

    Ok(())
}

#[test]
fn remove_key() -> Result<()> {
    let db_opener = temp_db_opener();
    let store = db_opener();
    store.set(b"key1", b"value1")?;
    assert!(store.remove(b"key1").is_ok());
    assert_eq!(store.get(b"key1")?, None);
    Ok(())
}

// Insert data until total size of the directory decreases.
// Test data correctness after compaction.
#[test]
fn compaction() -> Result<()> {
    let temp_dir =
        TempDir::new().expect("unable to create temporary working directory");
    let store = KvStore::open(temp_dir.path(), Some(TEST_CONFIG_OPTIONS))?;

    let dir_size = || {
        let entries = WalkDir::new(temp_dir.path()).into_iter();
        let len: walkdir::Result<u64> = entries
            .map(|res| {
                res.and_then(|entry| entry.metadata())
                    .map(|metadata| metadata.len())
            })
            .sum();
        len.expect("fail to get directory size")
    };

    let mut current_size = dir_size();
    for iter in 0..1000 {
        for key_id in 0..1000 {
            store.set(&key(key_id), format!("{}", iter).as_bytes())?;
        }

        let new_size = dir_size();
        if new_size > current_size {
            current_size = new_size;
            continue;
        }
        // Compaction triggered

        drop(store);
        let store = KvStore::open(temp_dir.path(), None)?;

        for key_id in 0..1000 {
            let k = key(key_id);
            let v = format!("{}", iter).into_bytes();
            assert_eq!(store.get(&k)?, Some(v));
        }
        return Ok(());
    }

    panic!("No compaction detected");
}

#[test]
fn concurrent_set() -> Result<()> {
    let db_opener = temp_db_opener();
    let store = db_opener();

    let barrier = Arc::new(Barrier::new(1001));
    for i in 0..1000 {
        let store = store.clone();
        let barrier = barrier.clone();
        thread::spawn(move || {
            store.set(&key(i), format!("value{}", i).as_bytes()).unwrap();
            barrier.wait();
        });
    }
    barrier.wait();

    for i in 0..1000 {
        assert_eq!(
            store.get(&key(i))?,
            Some(format!("value{}", i).into_bytes())
        );
    }

    drop(store);
    let store = db_opener();

    for i in 0..1000 {
        assert_eq!(
            store.get(&key(i))?,
            Some(format!("value{}", i).into_bytes())
        );
    }

    Ok(())
}

#[test]
fn concurrent_get() -> Result<()> {
    let db_opener = temp_db_opener();
    let store = db_opener();
    for i in 0..100 {
        store.set(&key(i), format!("value{}", i).as_bytes()).unwrap();
    }

    let mut handles = Vec::new();
    for thread_id in 0..1 {
        let store = store.clone();
        let handle = thread::spawn(move || {
            for i in 0..100 {
                let key_id = (i + thread_id) % 100;
                assert_eq!(
                    store.get(&key(key_id)).unwrap(),
                    Some(format!("value{}", key_id).into_bytes())
                );
            }
        });
        handles.push(handle);
    }
    for handle in handles {
        handle.join().unwrap();
    }

    drop(store);
    let store = db_opener();
    let mut handles = Vec::new();

    for thread_id in 0..100 {
        let store = store.clone();
        let handle = thread::spawn(move || {
            for i in 0..100 {
                let key_id = (i + thread_id) % 100;
                assert_eq!(
                    store.get(&key(key_id)).unwrap(),
                    Some(format!("value{}", key_id).into_bytes())
                );
            }
        });
        handles.push(handle);
    }
    for handle in handles {
        handle.join().unwrap();
    }

    Ok(())
}

fn key(n: u64) -> Vec<u8> {
    key_with_prefix("key/", n)
}

fn key_with_prefix(prefix: &str, n: u64) -> Vec<u8> {
    let mut key = prefix.as_bytes().to_vec();
    let bytes = n.to_be_bytes();
    key.extend_from_slice(&bytes);
    key
}
